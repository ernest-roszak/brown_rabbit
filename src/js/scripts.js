const btn = $(".card__btn--js");
const paragraph = $(".card__paragraph--js");
const hamburger = document.querySelector('.hamburger--js');
const navigation = document.querySelector('.navigation--js');
const searchText = $(".search--js");
const pageContent = $('body').text();

let currentBanner = $('.header__banner--active');
let nextBanner = currentBanner.next();
let counter = 1

const numberOfItems = $(".card__section .item").length;
const limitOfItems = 3;
const numberOfPages = Math.round(numberOfItems / limitOfItems);

$(".card__section .item:gt(" +  (limitOfItems -1) + ")").hide();
$(".pagination").append("<li class='page-item active'><a class='page-link' href='javascript:void(0)'>" + 1 + "</a></li>");

for(let i = 2; i <= numberOfPages; i++) {
    $(".pagination").append("<li class='page-item'><a class='page-link' href='javascript:void(0)'>" + i + "</a></li>");
}
$(".pagination").append("<li id='next' ><a class='page-link' href='javascript:void(0)' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>");

$(".pagination li.page-item").on("click", function() {
    if($(this).hasClass("active")){
        return false;
    } else {
        const currentPage = $(this).index();
        $(".pagination li").removeClass("active");
        $(this).addClass("active");
        $(".card__section .item").hide();
        const total = limitOfItems * currentPage;

        for(let i = (total - limitOfItems); i < total; i++){
            $(".card__section .item:eq(" + i + ")").show();
        }
    }
});

$("#next").on("click", function() {
    let currentPage = $(".pagination li.active").index();
    if(currentPage === numberOfPages) {
        return false;
    } else {
        currentPage++;
        $(".pagination li").removeClass("active");
        $(".card__section .item").hide();
        const total = limitOfItems * currentPage;

        for(let i = (total - limitOfItems); i < total; i++){
            $(".card__section .item:eq(" + i + ")").show();
        }
        $(".pagination li.page-item:eq(" + (currentPage - 1) + ")").addClass("active");
    }
})
$("#prev").on("click", function() {
    let currentPage = $(".pagination li.active").index();
    if(currentPage === 1) {
        return false;
    } else {
        currentPage--;
        $(".pagination li").removeClass("active");
        $(".card__section .item").hide();
        const total = limitOfItems * currentPage;

        for(let i = (total - limitOfItems); i < total; i++){
            $(".card__section .item:eq(" + i + ")").show();
        }
        $(".pagination li.page-item:eq(" + (currentPage - 1) + ")").addClass("active");
    }
})

function slide(){
    if(counter < 3) {
        currentBanner.removeClass('header__banner--active').css('z-index', -10);
        nextBanner.addClass('header__banner--active').css('z-index', 10);
        currentBanner = currentBanner.next();
        nextBanner = currentBanner.next();
        counter++
    }   else {
        currentBanner.removeClass('header__banner--active').css('z-index', -10);
        currentBanner = $('.firstBanner');
        currentBanner.addClass('header__banner--active').css('z-index', 10);
        nextBanner = currentBanner.next();
        counter = 1;
    }
}
const interval = setInterval(() => {
slide()
}, 5000)


const handleHamburger = () => {
    hamburger.classList.toggle('hamburger--active');
    navigation.classList.toggle('navigation--active');
}

btn.on('click', function() {
    $(this).parent().children(paragraph).toggleClass("card__paragraph--show");
});


function search() {
    const myText = searchText.val();
    if(pageContent.includes(myText)){
        const a = pageContent.search(myText);
        const b = pageContent.substr(pageContent.search(myText), myText.length);
        const background = "<span style='background-color: yellow'>" + b + "</span>";
        const result = pageContent.replace(b, background);
        };
    }



